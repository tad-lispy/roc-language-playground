name := roc-playground

include Makefile.d/defaults.mk

executables := $(patsubst src/%.roc, built/%, $(filter-out src/_%, $(wildcard src/*.roc)))

all: ## Build the program (DEFAULT)
all: test build
.PHONY: all

build: ## Build the program without testing
build: $(executables)
.PHONY: build


test:
	roc test
.PHONY: test

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: main
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive $</* $(prefix)/
.PHONY: install

built/%: src/%.roc
	mkdir --parents $(@D)
	command time --portability \
	  timeout --verbose 60s \
	  roc build --output built/ $<

### DEVELOPMENT

develop: ## Rebuild and run a development version of the program
develop:
	roc dev
.PHONY: develop

clean: ## Remove all build artifacts
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
