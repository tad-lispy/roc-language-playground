app "bug-compiles-forever-hogs-cpu"
    packages { cli: "https://github.com/roc-lang/basic-cli/releases/download/0.5.0/Cufzl36_SnJ4QbOoEmiJ5dIpUxBvdB3NEySvuH82Wio.tar.br" }
    imports [pf.Stdout]
    provides [main] to cli

main =
    Stdout.line "I wish to say hello. Alas."
