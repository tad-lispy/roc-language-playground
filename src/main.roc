app "hello"
    packages { pf: "https://github.com/roc-lang/basic-cli/releases/download/0.5.0/Cufzl36_SnJ4QbOoEmiJ5dIpUxBvdB3NEySvuH82Wio.tar.br" }
    imports [pf.Stdout]
    provides [main] to pf

main =
    actual = 2.0 + 3.0
    expected = 4.0
    if
        actual == expected
    then
        Stdout.line "Actually it's what I expected."
    else
        Stdout.line "What an unexpected result!"

# TODO: report that it segfaults when expectation fails, but only with fractions. Integers fail as expected.
expect
    actual = 2.0 + 2.0
    expected = 4.0
    actual == expected
