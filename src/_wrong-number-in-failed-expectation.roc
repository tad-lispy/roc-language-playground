app "wrong-number-in-failed-expectation"
    packages { cli: "https://github.com/roc-lang/basic-cli/releases/download/0.5.0/Cufzl36_SnJ4QbOoEmiJ5dIpUxBvdB3NEySvuH82Wio.tar.br" }
    imports [cli.Stdout]
    provides [main] to cli

main =
    Stdout.line "Run 'roc test' to see a strange result"


# Wrong

expect
    expected : I8
    expected = 1
    actual = 0
    expected == actual

expect
    expected : I16
    expected = 1
    actual = 0
    expected == actual

expect
    expected : I32
    expected = 1
    actual = 0
    expected == actual


# Correct with long integers

expect
    expected : I64
    expected = 1
    actual = 0
    expected == actual

expect
    expected : I128
    expected = 1
    actual = 0
    expected == actual


# Correct with two annotations

expect
    expected : I8
    expected = 1
    actual : I8
    actual = 0
    expected == actual

expect
    expected : I16
    expected = 1
    actual : I16
    actual = 0
    expected == actual

expect
    expected : I32
    expected = 1
    actual : I32
    actual = 0
    expected == actual

expect
    expected : I64
    expected = 1
    actual : I64
    actual = 0
    expected == actual

expect
    expected : I128
    expected = 1
    actual : I128
    actual = 0
    expected == actual

# Correct without annotations

expect
    expected = 1
    actual = 0
    expected == actual

expect
    expected = 1
    actual = 0
    expected == actual

expect
    expected = 1
    actual = 0
    expected == actual

expect
    expected = 1
    actual = 0
    expected == actual

expect
    expected = 1
    actual = 0
    expected == actual
