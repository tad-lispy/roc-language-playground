app "command-line-arguments"
    packages { cli: "https://github.com/roc-lang/basic-cli/releases/download/0.5.0/Cufzl36_SnJ4QbOoEmiJ5dIpUxBvdB3NEySvuH82Wio.tar.br" }
    imports [cli.Stdout, cli.Task, cli.Arg]
    provides [main] to cli

main =
    args <- Task.await Arg.list
    output = Str.joinWith args "\n"
    Stdout.line output
