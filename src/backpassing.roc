app "backpassing"
    packages { cli: "https://github.com/roc-lang/basic-cli/releases/download/0.5.0/Cufzl36_SnJ4QbOoEmiJ5dIpUxBvdB3NEySvuH82Wio.tar.br" }
    imports [cli.Stdout]
    provides [main] to cli

identity : a -> a
identity = \whatever -> whatever

# The weirdCount function takes a string and counts graphemes,
# but instead of returing the number,
# it passes it to another function (a callback)
# and returns whatever the callback returns
weirdCount : Str, (Nat -> a) -> a
weirdCount = \input, callback ->
    input
    |> Str.countGraphemes
    |> callback

expect
    actual = (weirdCount "Hello, World!" identity)
    expected = 13
    actual == expected

# explain is using backpassing to extract the number of graphemes from weirdCount
# then returns a string that explains how many graphemes are in it's input
explain : Str -> Str
explain = \input ->
    number <- weirdCount input
    "The string \"\(input)\" has \(Num.toStr number) graphemes."

expect
    actual = (explain "dogs")
    expected = "The string \"dogs\" has 4 graphemes."
    actual == expected

main =
    "Cats look like this 🐈‍⬛"
    |> explain
    |> Stdout.line
