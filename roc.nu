# Like roc run, but doesn't pollute the source directory with binaries
export def "roc try" [
    source: path = src/main.roc     # The program's source file
    ...program_args: string         # Arguments passed to the program at runtime
] {
    let built = mktemp
    roc build --output $built $source
    echo ""
    echo "Running the program:"
    echo ""
    ^$built $program_args
}
