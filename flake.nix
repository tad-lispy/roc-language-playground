{
  description = "Playing with https://www.roc-lang.org/";

  inputs = {
    roc.url = "github:roc-lang/roc";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, roc, nixpkgs }:
    let
      project_name = "roc-playground";
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
      rocPkgs = roc.packages.${system};
    in {
      devShells.${system}.default = pkgs.mkShell {
        name = "${project_name}-development-shell";
        packages =  [
          rocPkgs.default
          rocPkgs.lang-server
          pkgs.cachix
          pkgs.jq
        ];
      };
      # This doesn't work for now. See:
      # https://roc.zulipchat.com/#narrow/stream/231634-beginners/topic/Pure.20Nix.20build.20and.20platform.20fetching
      #
      # packages.${system}.default = pkgs.stdenv.mkDerivation {
      #   name = ${project_name};
      #   src = ./.;
      #   buildInputs = [
      #     pkgs.gnumake
      #     rocPkgs.default
      #   ];
      # };
    };
}
